describe('Suite de pruebas Cypress - Ejecución de formulario', function() {
  it('Verificar mensaje de respuesta de envío de formulario', function(){
    // visitamos la url del formulario
    cy.visit('https://goo.gl/vGvomp')
    //Obtenemos el input del correo electrónico y escribimos un correo
    cy.get('input').first().type('dmendieta@correo.net')
    //Obtenemos el div que cotiene el input del nombre
    cy.get('div[data-item-id="1449008445"]').within(() => {
      // Obtenemos el input del nombre y escribimos un nombre
      cy.get('input').type('David')
    })
    // Obtenemos el div que contiene el input de los apellidos
    cy.get('div[data-item-id="808464754"]').within(() => {
      // Obtenemos el input de los apellidos y escribimos los apellidos
      cy.get('input').type('Mendieta Lopez')
    })
    // Obtenemos el div que contiene el input del teléfono
    cy.get('div[data-item-id="636713294"]').within(() => {
      // Obtenemos el input del teléfono y escribimos un número
      cy.get('input').type('4971245822')
    })
    // Obtenemos el botón de enviar y hacemos clic
    cy.contains('Enviar').click()
    // Verificamos que se haya mostrado el mensaje de registro del formulario
    cy.get('.freebirdFormviewerViewResponseConfirmationMessage').should('contain','Se ha registrado tu respuesta')
  })
})
